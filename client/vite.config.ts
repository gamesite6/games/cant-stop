import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/dice-climbers/",
  build: {
    emptyOutDir: false,
    lib: {
      entry: "./src/main.ts",
      fileName: "dice-climbers",
      name: "Gamesite6_DiceClimbers",
    },
  },
  plugins: [
    vue({
      template: {
        compilerOptions: {
          isCustomElement(tag) {
            return (
              tag === "gs6-user" ||
              tag === "gs6-player-box" ||
              tag === "gs6-die"
            );
          },
        },
      },
    }),
  ],
});
