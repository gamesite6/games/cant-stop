import { withUserFirst } from "./util";

describe("utils", () => {
  test("withUserFirst", () => {
    const players = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];
    const result = withUserFirst(players, 3);

    expect(result.map((p) => p.id)).toEqual([3, 4, 1, 2]);
  });
});
