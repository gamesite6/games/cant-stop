export function withUserFirst<T extends { id: PlayerId }>(
  players: T[],
  userId: PlayerId | undefined
): T[] {
  const idx = players.findIndex((p) => p.id === userId);
  if (idx === -1) {
    return players;
  } else {
    return [...players.slice(idx), ...players.slice(0, idx)];
  }
}

export function isEmptyObject<K extends string | number | symbol, V>(
  obj: Record<K, V>
) {
  return Object.keys(obj).length === 0 && obj.constructor === Object;
}
