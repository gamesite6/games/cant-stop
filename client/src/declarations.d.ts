type PlayerId = number;
type Index = number;
type DiceValue = number;

type Player = {
  id: PlayerId;

  /**
   * Record containing how many steps (value)
   * the player has taken for each dice pair value (key)
   */
  cones: Record<DiceValue, number>;
};

type GameState = {
  players: Player[];
  runners: Record<number, number>;
  phase: Phase;
};

type GameSettings = {
  playerCounts: number[];
};

type Action =
  | { name: "roll" }
  | { name: "pair"; dice: Array<Index> }
  | { name: "stop" };

type DiceRoll = 1 | 2 | 3 | 4 | 5 | 6;

type Phase =
  | { name: "rolling"; player: PlayerId }
  | {
      name: "pairing";
      player: PlayerId;
      dice: DiceRoll[];
    }
  | {
      name: "complete";
      player: undefined;
      winner: PlayerId;
    };

type GameProps = {
  initialState?: GameState;
  initialSettings?: GameSettings;
  initialPlayerId?: PlayerId;
  onAction: (action: Action) => void;
};

type GameData = {
  state: GameState;
  settings: GameSettings;
  playerId?: PlayerId;
};
