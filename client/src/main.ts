import * as Vue from "vue";
import GameComponent from "./components/Game.vue";
import ReferenceComponent from "./components/Reference.vue";
import SettingsComponent from "./components/Settings.vue";

type GameInstance = Vue.ComponentPublicInstance<GameProps, {}, GameData>;

export class Game extends HTMLElement {
  #state?: GameState;
  #settings?: GameSettings;
  #playerId?: PlayerId | undefined;
  #app?: Vue.App;
  #vm?: GameInstance;
  #handleAction: (action: Action) => void;

  constructor() {
    super();

    this.#handleAction = (action: Action) => {
      this.dispatchEvent(
        new CustomEvent("action", {
          detail: action,
        })
      );
    };
  }

  set state(state: GameState) {
    if (this.#state !== state) {
      this.#state = state;
      if (this.#vm !== undefined) {
        this.#vm.state = state;
      }
    }
  }

  set settings(settings: GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      if (this.#vm !== undefined) {
        this.#vm.settings = settings;
      }
    }
  }
  set playerid(playerId: PlayerId) {
    if (this.#playerId !== playerId) {
      this.#playerId = playerId;
      if (this.#vm !== undefined) {
        this.#vm.playerId = playerId;
      }
    }
  }

  connectedCallback() {
    const props: GameProps = {
      initialState: this.#state,
      initialSettings: this.#settings,
      initialPlayerId: this.#playerId,
      onAction: this.#handleAction,
    };
    this.#app = Vue.createApp(GameComponent, props);
    this.#vm = this.#app.mount(this) as GameInstance;
  }

  disconnectedCallback() {
    if (this.#app) {
      this.#app.unmount();

      this.#vm = undefined;
      this.#app = undefined;
    }
  }
}

type ReferenceProps = {
  initialSettings?: GameSettings;
};

type ReferenceData = {
  settings: GameSettings;
};

type ReferenceInstance = Vue.ComponentPublicInstance<
  ReferenceProps,
  {},
  ReferenceData
>;

export class Reference extends HTMLElement {
  #settings?: GameSettings;
  #app?: Vue.App;
  #vm?: ReferenceInstance;

  set settings(settings: GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      if (this.#vm !== undefined) {
        this.#vm.settings = settings;
      }
    }
  }

  connectedCallback() {
    const props: ReferenceProps = {
      initialSettings: this.#settings,
    };
    this.#app = Vue.createApp(ReferenceComponent, props);
    this.#vm = this.#app.mount(this) as ReferenceInstance;
  }

  disconnectedCallback() {
    if (this.#app) {
      this.#app.unmount();

      this.#vm = undefined;
      this.#app = undefined;
    }
  }
}

export { defaultSettings, playerCounts } from "./settings";

type SettingsProps = {
  initialSettings: GameSettings;
  initialReadonly: boolean;
};
export class Settings extends HTMLElement {
  #settings?: GameSettings;

  set settings(settings: GameSettings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      if (this.#vm !== undefined) {
        this.#vm.settings = settings;
      }
    }
  }

  get #readonly(): boolean {
    const attr = this.attributes.getNamedItem("readonly");
    return attr !== null && attr.value !== "false";
  }

  static get observedAttributes() {
    return ["readonly"];
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    if (name === "readonly" && this.#vm) {
      this.#vm.readonly = newValue;
    }
  }

  #app?: Vue.App;
  #vm?: Vue.ComponentPublicInstance<SettingsProps, {}, any>;

  connectedCallback() {
    const props = {
      initialSettings: this.#settings,
      initialReadonly: this.#readonly,
      onChange: (settings: GameSettings) => {
        this.dispatchEvent(
          new CustomEvent("settings_change", {
            detail: settings,
          })
        );
      },
    };

    this.#app = Vue.createApp(SettingsComponent, props);
    this.#vm = this.#app.mount(this);
  }
}
