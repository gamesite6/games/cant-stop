using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using PlayerId = System.Int32;
using Newtonsoft.Json;

namespace DiceClimbers.Controllers
{
  [ApiController]
  public class DiceClimbersController : ControllerBase
  {
    [HttpPost("info")]
    [Consumes("application/json")]
    [Produces("application/json")]
    public ActionResult postInfo(InfoReq req)
    {
      return Ok(new InfoRes(PlayerCounts: req.Settings.ValidatePlayerCounts()));
    }

    [HttpPost("initial-state")]
    [Consumes("application/json")]
    [Produces("application/json")]
    public ActionResult postInitialState(InitialStateReq req)
    {
      GameState? initialState = InitialState.Create(req.Players, req.Settings, req.Seed);
      if (initialState != null)
      {
        return Ok(new InitialStateRes(State: initialState));
      }
      else
      {
        return UnprocessableEntity();
      }
    }

    [HttpPost("perform-action")]
    [Consumes("application/json")]
    [Produces("application/json")]
    public ActionResult postPerformAction(PerformActionReq req)
    {
      GameState? nextState = PerformAction.NextState(
        state: req.State,
        action: req.Action,
        userId: req.PerformedBy,
        settings: req.Settings,
        seed: req.Seed
      );
      if (nextState != null)
      {
        return Ok(new PerformActionRes(nextState, nextState.Completed()));
      }
      else
      {
        return UnprocessableEntity();
      }
    }
  }

  public record InfoReq(
    Settings Settings
  );
  public record InfoRes(
    List<int> PlayerCounts
  );

  public record InitialStateReq(
    HashSet<PlayerId> Players,
    Settings Settings,
    int Seed
  );


  public record InitialStateRes(
    GameState State
  );


  public record PerformActionReq(
    [JsonConverter(typeof(ActionConverter))]
    IAction Action,
    PlayerId PerformedBy,
    GameState State,
    Settings Settings,
    int Seed
  );

  public record PerformActionRes(
    GameState NextState,
    bool Completed
  );
}
