using System;
using System.Collections.Generic;
using PlayerId = System.Int32;
using DiceValue = System.Int32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace DiceClimbers
{
  public interface IPhase
  {
    string Name { get; }
  }

  public sealed class Rolling : IPhase
  {
    public const string PhaseName = "rolling";
    public string Name => PhaseName;

    public PlayerId Player { get; set; }

    public Rolling(PlayerId playerId)
    {
      Player = playerId;
    }
  }

  public sealed class Pairing : IPhase
  {
    public const string PhaseName = "pairing";
    public string Name => PhaseName;
    public IList<DiceValue> Dice { get; set; }
    public PlayerId Player { get; set; }

    public Pairing(PlayerId playerId, IList<DiceValue> dice)
    {
      Player = playerId;
      Dice = dice;
    }
  }

  public sealed class GameComplete : IPhase
  {
    public const string PhaseName = "complete";
    public string Name => PhaseName;

    public PlayerId Winner { get; set; }
    public GameComplete(PlayerId winner)
    {
      Winner = winner;
    }
  }

  public sealed class PhaseConverter : JsonConverter<IPhase>
  {
    public override void WriteJson(JsonWriter writer, IPhase? value, JsonSerializer serializer)
    {
      serializer.Serialize(writer, value);
    }

    public override IPhase ReadJson(
      JsonReader reader,
      Type objectType,
      IPhase? existingValue,
      bool hasExistingValue,
      JsonSerializer serializer)
    {
      var jsonObject = JObject.Load(reader);

      IPhase? result = null;
      switch (jsonObject["name"]?.Value<string>())
      {
        case Rolling.PhaseName:
          result = serializer.Deserialize<Rolling>(jsonObject.CreateReader());
          break;
        case Pairing.PhaseName:
          result = serializer.Deserialize<Pairing>(jsonObject.CreateReader());
          break;
        case GameComplete.PhaseName:
          result = serializer.Deserialize<GameComplete>(jsonObject.CreateReader());
          break;
      }

      if (result != null)
      {
        return result;
      }
      else
      {
        throw new FormatException("invalid action json");
      }
    }
  }
}
