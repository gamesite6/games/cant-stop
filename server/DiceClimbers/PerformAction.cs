using System;
using System.Linq;
using System.Collections.Generic;
using PlayerId = System.Int32;
using Index = System.Int32;

namespace DiceClimbers
{
  public static class PerformAction
  {
    public static bool IsAllowed(GameState state, IAction action, PlayerId userId, Settings settings)
    {
      switch (state.Phase)
      {
        case Rolling phase:
          switch (action)
          {
            case Roll rollAction:
            case Stop stopAction:
              return phase.Player == userId;

            default: return false;
          }
        case Pairing phase:
          switch (action)
          {
            case Pair pairAction:
              return phase.Player == userId &&
                pairAction.Dice.Count == 2 &&
                pairAction.Dice.All(diceIdx => diceIdx >= 0 && diceIdx <= 3);

            default: return false;
          }
        case GameComplete _: return false;
        default: throw new NotImplementedException();
      }
    }

    public static GameState? NextState(GameState state, IAction action, PlayerId userId, Settings settings, int seed)
    {
      if (!IsAllowed(state, action, userId, settings))
      {
        return null;
      }

      var rng = new Random(seed);

      switch (state.Phase)
      {
        case Rolling rollingPhase:
          switch (action)
          {
            case Roll _:
              var dice = new List<int>() {
                Dice.RollD6(rng),
                Dice.RollD6(rng),
                Dice.RollD6(rng),
                Dice.RollD6(rng),
              };
              state.Phase = new Pairing(playerId: userId, dice);
              return state;
            case Stop _:
              var user = state.Players.Find(p => p.Id == userId);
              if (user is null) return null;

              foreach (var (diceValue, steps) in state.Runners)
              {
                var previousSteps = 0;
                user.Cones.TryGetValue(diceValue, out previousSteps);
                user.Cones[diceValue] = previousSteps + steps;

                if (user.Cones[diceValue] >= GameState.MaxSteps(diceValue))
                {
                  foreach (var otherPlayer in state.Players.Where(p => p.Id != userId))
                  {
                    otherPlayer.Cones.Remove(diceValue);
                  }
                }
              }

              state.Runners = new Dictionary<int, int>();

              var userHasWon = user.Cones.Where(kv =>
                {
                  var (diceValue, steps) = kv;
                  return steps >= GameState.MaxSteps(diceValue);
                }).Count() >= 3;

              if (userHasWon)
              {
                state.Phase = new GameComplete(winner: userId);
              }
              else
              {
                state.Phase = new Rolling(state.NextPlayerId(userId));
              }

              return state;

            default: return null;
          }
        case Pairing pairingPhase:
          switch (action)
          {
            case Pair pairAction:
              var user = state.Players.Find(p => p.Id == userId);
              if (user is null) return null;

              var diceValues = new List<int>() {
                pairAction.Dice
                  .Select(diceIdx => pairingPhase.Dice[diceIdx]).Sum(),
                new HashSet<Index>{ 0,1,2,3 }.Except(pairAction.Dice)
                  .Select(diceIdx => pairingPhase.Dice[diceIdx]).Sum(),
              };

              bool addedRunner = false;
              foreach (var diceValue in diceValues)
              {
                var startingPosition = 0;
                user.Cones.TryGetValue(diceValue, out startingPosition);
                var steps = state.AddRunner(
                  diceValue,
                  startingPosition
                );

                if (steps != null)
                {
                  addedRunner = true;
                }
              }

              if (addedRunner)
              {
                state.Phase = new Rolling(playerId: userId);
              }
              else
              {
                state.Runners = new Dictionary<int, int>();
                state.Phase = new Rolling(state.NextPlayerId(userId));
              }
              return state;

            default: return null;
          }
        default: return null;
      }
    }
  }
}
