using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Index = System.Int32;

namespace DiceClimbers
{
  public interface IAction
  {
    string Name { get; }
  }

  public sealed class Roll : IAction
  {
    public const string ActionName = "roll";
    public string Name => ActionName;
  }

  public sealed class Pair : IAction
  {
    public const string ActionName = "pair";
    public string Name => ActionName;

    public HashSet<Index> Dice { get; set; }
    public Pair(HashSet<Index> diceIndices)
    {
      Dice = diceIndices;
    }

  }

  public sealed class Stop : IAction
  {
    public const string ActionName = "stop";
    public string Name => ActionName;
  }

  public sealed class ActionConverter : JsonConverter<IAction>
  {
    public override void WriteJson(JsonWriter writer,
        IAction? value, JsonSerializer serializer)
    {
      serializer.Serialize(writer, value);
    }
    public override IAction ReadJson(
      JsonReader reader,
      Type objectType,
      IAction? existingValue,
      bool hasExistingValue,
      JsonSerializer serializer)
    {
      var jsonObject = JObject.Load(reader);

      IAction? result = null;
      switch (jsonObject["name"]?.Value<string>())
      {
        case Roll.ActionName:
          result = serializer.Deserialize<Roll>(jsonObject.CreateReader());
          break;
        case Pair.ActionName:
          result = serializer.Deserialize<Pair>(jsonObject.CreateReader());
          break;
        case Stop.ActionName:
          result = serializer.Deserialize<Stop>(jsonObject.CreateReader());
          break;
      }

      if (result != null)
      {
        return result;
      }
      else
      {
        throw new FormatException("invalid action json");
      }
    }
  }
}
