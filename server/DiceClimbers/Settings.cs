using System.Collections.Generic;

namespace DiceClimbers
{

  public record struct Settings(
    List<int> PlayerCounts
  )
  {
    static List<int> ValidPlayerCounts = new List<int> { 2, 3, 4 };

    public List<int> ValidatePlayerCounts()
    {
      var self = this;
      return ValidPlayerCounts.FindAll(c => self.PlayerCounts.Contains(c));
    }
  }
}
