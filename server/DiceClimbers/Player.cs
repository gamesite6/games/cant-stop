using System.Collections.Generic;
using PlayerId = System.Int32;
using DiceValue = System.Int32;

namespace DiceClimbers
{
  public sealed class Player
  {
    public PlayerId Id { get; set; }
    public Dictionary<DiceValue, int> Cones;

    public Player(PlayerId id)
    {
      Id = id;
      Cones = new Dictionary<int, int>();
    }

    private static int? DiceValueToIndex(int diceValue)
    {
      if (diceValue >= 2 && diceValue <= 12)
      {
        return diceValue - 1;
      }
      else
      {
        return null;
      }
    }
  }
}
