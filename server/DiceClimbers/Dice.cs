using DiceValue = System.Int32;
using System;

namespace DiceClimbers
{
  public static class Dice
  {
    public static DiceValue RollD6(Random rng)
    {
      return rng.Next(1, 7);
    }
  }
}
