using System;
using System.Collections.Generic;
using System.Linq;
using PlayerId = System.Int32;

namespace DiceClimbers
{
  public static class InitialState
  {
    public static GameState? Create(HashSet<PlayerId> players, Settings settings, int seed)
    {
      var rng = new Random(seed);

      if (settings.PlayerCounts.Contains(players.Count()))
      {
        var playerList = players.Select(playerId => new Player(id: playerId)).ToList();

        var firstPlayerIdx = Math.Abs(rng.Next() % playerList.Count);
        var firstPlayer = playerList[firstPlayerIdx];

        return new GameState(
          playerList,
          phase: new Rolling(playerId: firstPlayer.Id)
        );
      }
      else
      {
        return null;
      }
    }
  }
}
