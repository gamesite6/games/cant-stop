using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

using DiceValue = System.Int32;
using PlayerId = System.Int32;
using Runners = System.Collections.Generic.Dictionary<int, int>;

namespace DiceClimbers
{
  public sealed class GameState
  {
    public List<Player> Players { get; set; }
    public Runners Runners { get; set; }

    [JsonConverter(typeof(PhaseConverter))]
    public IPhase Phase { get; set; }

    public GameState(List<Player> players, IPhase phase)
    {
      Players = players;
      Phase = phase;
      Runners = new Dictionary<DiceValue, int>();
    }

    public bool Completed()
    {
      return false; // TODO
    }

    public PlayerId NextPlayerId(PlayerId currentPlayerId)
    {
      var currentPlayerIndex = Players.FindIndex(p => p.Id == currentPlayerId);
      if (currentPlayerIndex == -1)
      {
        return Players.First()?.Id ?? currentPlayerId;
      }
      else
      {
        var nextPlayerIndex = (currentPlayerIndex + 1) % Players.Count;
        return Players[nextPlayerIndex].Id;
      }
    }

    public static int MaxSteps(DiceValue diceValue)
    {
      return 13 - Math.Abs(diceValue - 7) * 2;
    }

    public HashSet<DiceValue> CompletedColumns()
    {
      return Players
        .SelectMany(p => p.Cones)
        .Where(kv =>
        {
          var (diceValue, steps) = kv;
          return MaxSteps(diceValue) == steps;
        })
        .Select(kv => kv.Key)
        .ToHashSet<DiceValue>();
    }

    public int? AddRunner(DiceValue diceValue, int startingPosition)
    {
      if (diceValue < 2 || diceValue > 12 || CompletedColumns().Contains(diceValue)) return null;

      if (Runners.ContainsKey(diceValue) || Runners.Count < 3)
      {
        var steps = 0;
        Runners.TryGetValue(diceValue, out steps);
        if (startingPosition + steps >= MaxSteps(diceValue)) return null;

        steps += 1;
        Runners[diceValue] = steps;
        return startingPosition + steps;
      }
      else
      {
        return null;
      }
    }
  }
}
