using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace DiceClimbers
{
  public class Program
  {
    public static void Main(string[] args)
    {
      ushort port;
      if (!ushort.TryParse(Environment.GetEnvironmentVariable("PORT"), out port))
      {
        throw new FormatException("invalid PORT env var");
      }

      CreateHostBuilder(args, port).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args, ushort port) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
              webBuilder.UseStartup<Startup>();
              webBuilder.UseUrls($"http://*:{port}");
            });
  }
}
